package com.epn.edu.ac;

public class Palindromo {
	
	public String palabraInvertida(String palabraOrinigal) {
		String inversionDePalabra="";
		for (int i = palabraOrinigal.length(); i >0 ; i--) {
			inversionDePalabra+=palabraOrinigal.charAt(i-1);
		}
		return inversionDePalabra;
	}
	public void siPalabraEsPolindromo(String palabraOrinigal) {
		if (palabraInvertida(palabraOrinigal).equalsIgnoreCase(palabraOrinigal)) {
			System.out.println("Es una palabra palíndroma");
			
		}else {
			System.out.println("No es una palabra palíndroma");	
		}
	}
	

}
